# AirGradient to Prometheus gateway

This program ingests samples sent from [AirGradient sensors](https://www.airgradient.com/) and then
presents them as a [Prometheus](https://prometheus.io/) exporter.

I use this to draw pretty graphs in Grafana of the various sensors I
have setup to monitor atmospheric conditions (temperature, humidity)
and air quality ([particulates](https://en.wikipedia.org/wiki/Particulates), [CO2](https://en.wikipedia.org/wiki/Carbon_dioxide), etc).

# Installation

Install the [bottle](https://bottlepy.org/) framework alongside the [Prometheus client](https://prometheus.github.io/client_python/)
library. Tested on Python 3.11, but should be relatively portable.

You can install the program manually in `/usr/local/bin` with:

    install -m 555 airgradient-exporter.py /usr/local/bin/airgradient-exporter

There's a systemd service you can just install (e.g. in
`/etc/systemd/system`) and enable:

    cp airgradient-exporter /etc/systemd/system
    systemctl enable --now airgradient-exporter

The service should now be running, inspect the logs with:

    journalctl -f -u airgradient-exporter

Alternatively, you can start the service by hand, with:

    airgradient-exporter

A common configuration is to change the listening port, for example
you might start with this if you do not have another webserver:

    sudo airgradient-exporter --receiver-port 80

... or if, like in my configuration, you have trouble with IPv6, you
might want to change the listening address:

    airgradient-exporter --receiver-host 127.0.0.1

Same applies to the Prometheus exporter configuration, with
`--prometheus-port` and `--prometheus-host`.

You can raise the logging level with `--verbose` or `--debug`.

## Reverse proxy configuration

Then you somehow need to direct your sensors towards that exporter. By
default, it runs on a port the sensors will not expect, so you might
need to setup a reverse proxy in front of it, at the very least. This
is what I did in Apache 2.4:

    <VirtualHost *:80>
        ServerName ag-hw.example.com
        ServerAlias hw.airgradient.com

        DocumentRoot /var/www/html/
        ProxyPreserveHost On

        ProxyPass "/" "http://127.0.0.1:10241/"
        ProxyPassReverse "/" "http://127.0.0.1:10241/"
        RequestHeader set X-Forwarded-Proto http
        RequestHeader set X-Forwarded-Prefix /

        <Location />
        Require ip 10.0.0.42
        </Location>
    </VirtualHost>

Note the `Require ip` there: if you do not have a line like this,
*anyone* can hijack your exporter and start injecting samples in your
Prometheus server, which would be Bad.

## DNS reconfiguration

Now you need to point your sensors to the above. The easier way, if
you control it, is to hijack the `hw.airgradient.com` endpoint which
the AirGradient firmware talk to. There's no authentication, so just
injecting an entry in DNS might work. If your home router runs
`dnsmasq`, this configuration will make it work, assuming `10.0.0.2`
is the IP address of the above web server:

    address=/hw.airgradient.com/10.0.0.2

Note that there's a one hour TTL (Time To Live) on that DNS record, so
the routers will take some time to switch over.

## Flashing firmware

If you do *not* control DNS, you *can* flash the AirGradient
firmware. It's, from my perspective, a more involved procedure, but if
you're familiar with the [Arduino](https://www.arduino.cc/) IDE, that might be more like
your cup of tea.

In that case, you need to change `APIROOT` variable to point to your
web server, in the above reverse proxy configuration, that would be
`ag-hw.example.com`. That entry, of course, also needs to exist in DNS
but you might get away with using an IP address here, untested. Using
an IP address would imply changes to the reverse proxy configuration
as well, of course, since the above configuration is "name-based".

## Scraping from Prometheus

The last step is to make sure a Prometheus server scrapes the
exporter. This can be done with a configuration like this, in
`prometheus.yml`:

    - job_name: airgradient-exporter
        static_configs:
          - targets: ['localhost:10240']
        metric_relabel_configs:
          - source_labels: ['device_id']
            regex: 'deadbeef'
            target_label: 'device_name'
            replacement: 'indoors'
          - source_labels: ['device_id']
            regex: '42ebaba'
            target_label: 'device_name'
            replacement: 'office'
          - source_labels: ['device_id']
            regex: '172eef73a(.*)'
            target_label: 'device_name'
            replacement: 'outdoors$1'

Note that, in the above, we add a new `device_name` label to turn the
machine `device_id` label into a human-readable name. The last one has
a match group (`(.*)`) because it is an Open Air sensor with multiple
metrics. You could also decide to simply drop those metrics with:

    - job_name: airgradient-exporter
        static_configs:
          - targets: ['localhost:10240']
        metric_relabel_configs:
          - source_labels: ['device_id']
            regex: '.*_.*'
            action: drop

# Exported metrics

Here's an example dump of the exported metrics:

    # HELP airquality_wifi WiFi Signal Strength
    # TYPE airquality_wifi gauge
    airquality_wifi{device_id="deadbeef"} -75.0
    airquality_wifi{device_id="42ebaba"} -81.0
    airquality_wifi{device_id="172eef73a"} -76.0
    # HELP airquality_rco2 CO2 Concentration
    # TYPE airquality_rco2 gauge
    airquality_rco2{device_id="deadbeef"} 501.0
    airquality_rco2{device_id="42ebaba"} 509.0
    airquality_rco2{device_id="172eef73a"} -1.0
    # HELP airquality_pm01 PM 0.1 Concentration
    # TYPE airquality_pm01 gauge
    airquality_pm01{device_id="42ebaba"} 5.0
    airquality_pm01{device_id="172eef73a"} 23.0
    airquality_pm01{device_id="172eef73a_1"} 21.9
    airquality_pm01{device_id="172eef73a_2"} 24.0
    # HELP airquality_pm02 PM 2.5 Concentration
    # TYPE airquality_pm02 gauge
    airquality_pm02{device_id="deadbeef"} 10.0
    airquality_pm02{device_id="42ebaba"} 9.0
    airquality_pm02{device_id="172eef73a"} 35.2
    airquality_pm02{device_id="172eef73a_1"} 33.0
    airquality_pm02{device_id="172eef73a_2"} 37.3
    # HELP airquality_pm10 PM 10 Concentration
    # TYPE airquality_pm10 gauge
    airquality_pm10{device_id="42ebaba"} 8.0
    airquality_pm10{device_id="172eef73a"} 37.0
    airquality_pm10{device_id="172eef73a_1"} 35.7
    airquality_pm10{device_id="172eef73a_2"} 38.5
    # HELP airquality_pm003_count PM 0.3 Count
    # TYPE airquality_pm003_count gauge
    airquality_pm003_count{device_id="42ebaba"} 1281.0
    airquality_pm003_count{device_id="172eef73a"} 3085.0
    airquality_pm003_count{device_id="172eef73a_1"} 3051.0
    airquality_pm003_count{device_id="172eef73a_2"} 3119.0
    # HELP airquality_tvoc_index TVOC Index
    # TYPE airquality_tvoc_index gauge
    airquality_tvoc_index{device_id="deadbeef"} 120.0
    airquality_tvoc_index{device_id="42ebaba"} 17.0
    airquality_tvoc_index{device_id="172eef73a"} -1.0
    # HELP airquality_nox_index NOx Index
    # TYPE airquality_nox_index gauge
    airquality_nox_index{device_id="deadbeef"} 2.0
    airquality_nox_index{device_id="42ebaba"} 1.0
    airquality_nox_index{device_id="172eef73a"} -1.0
    # HELP airquality_atmp Temperature
    # TYPE airquality_atmp gauge
    airquality_atmp{device_id="deadbeef"} 20.86
    airquality_atmp{device_id="42ebaba"} 21.23
    airquality_atmp{device_id="172eef73a"} 3.8
    airquality_atmp{device_id="172eef73a_1"} 3.8
    airquality_atmp{device_id="172eef73a_2"} 3.9
    # HELP airquality_rhum Relative Humidity
    # TYPE airquality_rhum gauge
    airquality_rhum{device_id="deadbeef"} 29.0
    airquality_rhum{device_id="42ebaba"} 32.0
    airquality_rhum{device_id="172eef73a"} 42.8
    airquality_rhum{device_id="172eef73a_1"} 43.7
    airquality_rhum{device_id="172eef73a_2"} 42.0
    # HELP airquality_boot Boot count
    # TYPE airquality_boot gauge
    airquality_boot{device_id="172eef73a"} 33.0

Note that some sensors send *two* set of metrics on each push, which
is a bit confusing. Those are labeled with an underscore in the
output.

This has been tested on three distinct devices:

 * DIY Pro
 * DIY Pro 4.2
 * Open Air Outdoor Monitor

If you look closely, you might be able to guess which is which in the
above (redacted) output.

# Credits

This was inspired by [this exporter](https://gitlab.com/junialter/airgradient-to-prometheus) (deleted, see [my fork](https://gitlab.com/anarcat/airgradient-exporter)) and is very
similar. It was rewritten from the ground up when I realized I was
going to just change everything eventually. I also wanted to
experiment with another framework than Flask because I was getting
tired of this warning from Flask:

    INFO:werkzeug:WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.

Arguably, that's petty and a bit ridiculous; bottle just replaces
werkzeug by its own limited WSGI handler, but I'm not running a crazy
operation here, so for now this is good enough. I have also found
Bottle to be slightly easier to use (particularly because you don't
need to wrap return values).

# Future work

The multiple metrics per device thing is weird and should probably be
fixed or tweaked.

The grafana dashboards should be shared here.

The X-Forwarded-From headers could be taken into account.

The firmware-related endpoints *could* be forwarded back upstream.

After completing this project, I was pointed at a [library to handle
incoming measurements](https://github.com/just-oblivious/pygradient), basically only the `measures`
endpoint. It's async and nicely designed. It could replace the
`receive_data` endpoint, but not the Prometheus bits. See [upstream
discussion for next steps](https://github.com/just-oblivious/pygradient/issues/1).
