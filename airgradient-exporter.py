#!/usr/bin/python3

# Copyright (c) 2024 Antoine Beaupré
# Copyright (c) 2024 Jochen Demmer
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import argparse
import logging

from bottle import request, run, get, post, put
from prometheus_client import start_http_server, Gauge


metrics_descriptions = {
    "wifi": "WiFi Signal Strength",
    "rco2": "CO2 Concentration",
    "pm01": "PM 0.1 Concentration",
    "pm02": "PM 2.5 Concentration",
    "pm10": "PM 10 Concentration",
    "pm003_count": "PM 0.3 Count",
    "tvoc_index": "TVOC Index",
    "nox_index": "NOx Index",
    "atmp": "Temperature",
    "rhum": "Relative Humidity",
    "boot": "Boot count",
}

ignored_metrics = [
    "magic_record",
]
metrics = {}
for metric, desc in metrics_descriptions.items():
    metrics[metric] = Gauge("airquality_" + metric, desc, ["device_id"])


@post(r"/sensors/airgradient\:<device_id>/measures")
def receive_data(device_id):
    """main entry point for measurements

    TODO: port this to pyairgradient https://github.com/just-oblivious/pygradient

    At least before complicating or hacking the code here any further,
    consider yet another rewrite, or patch, on top of that API, just
    to export metrics.

    See also https://github.com/just-oblivious/pygradient/issues/1.
    """
    logging.debug("device %s, data: %s", device_id, request.json)
    submitted = []

    def process_metrics(device_id, key, value):
        if not metrics.get(key):
            logging.warning(
                "unknown key, value submitted by device %s: %s, %s",
                device_id,
                key,
                value,
            )
            return
        submitted.append(key)
        metrics[key].labels(device_id).set(value)

    for key, value in request.json.items():
        if key in ignored_metrics:
            continue
        if key == "channels":
            # Open Air has two sensors, record as two separate devices
            for i, m in value.items():
                d = f"{device_id}_{i}"
                for k, v in m.items():
                    process_metrics(d, k, v)
        else:
            process_metrics(device_id, key, value)
    # check that all metrics were provided
    missing = [key for key in metrics.keys() if key not in submitted]
    if missing:
        logging.info(
            "device %s does not have metrics %s",
            device_id,
            ", ".join(missing),
        )
    return {"status": "success"}


# /sensors/airgradient:84fce60d6bf1/config?attempt=2&fw=0.3.8
# example from hw.airgradient.com:
#
# {"schedule":{"pm02":180,"atmp":120,"rco2":120},"co2CalibrationRequested":false,"watchdogTestRequested":false,"ledTestRequested":false,"display":{"ledMode":"pm","ledCo2ModeThreshold1":1000,"ledCo2ModeThreshold2":2000,"ledCo2ModeRangeEnd":4000},"model":"UNKNOWN"}  # noqa: E501
@get(r"/sensors/airgradient\:<device_id>/config")
def get_config(device_id):
    return {
        "schedule": {"pm02": 180, "atmp": 120, "rco2": 120},
        "co2CalibrationRequested": False,
        "watchdogTestRequested": False,
        "ledTestRequested": False,
        "display": {
            "ledMode": "pm",
            "ledCo2ModeThreshold1": 1000,
            "ledCo2ModeThreshold2": 2000,
            "ledCo2ModeRangeEnd": 4000,
        },
        "model": "UNKNOWN",
    }


# input is (e.g.) '{"id":"0.3.8"}'
# we should really just send this back upstream
@put(r"/sensors/airgradient\:<device_id>/openair/firmware")
def put_config(device_id):
    return {"id": "0.3.8", "ota_id": "0.3.8"}


class LoggingAction(argparse.Action):
    """change log level on the fly

    The logging system should be initialized before this, using
    `basicConfig`."""

    def __init__(self, *args, **kwargs):  # type: ignore[no-untyped-def]
        """setup the action parameters

        This enforces a selection of logging levels. It also checks if
        const is provided, in which case we assume it's an argument
        like `--verbose` or `--debug` without an argument.
        """
        kwargs["choices"] = logging._nameToLevel.keys()
        if "const" in kwargs:
            kwargs["nargs"] = 0
        super().__init__(*args, **kwargs)

    def __call__(self, parser, ns, values, option):  # type: ignore[no-untyped-def]
        """if const was specified it means argument-less parameters"""
        if self.const:
            logging.getLogger("").setLevel(self.const)
        else:
            logging.getLogger("").setLevel(values)
        # cargo-culted from _StoreConstAction
        setattr(ns, self.dest, self.const or values)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Receive AirGradient data and export Prometheus metrics"
    )
    logging.basicConfig(level="WARNING", format="%(message)s")
    parser.add_argument(
        "-v",
        "--verbose",
        action=LoggingAction,
        const="INFO",
        help="enable verbose messages",
    )
    parser.add_argument(
        "-d",
        "--debug",
        action=LoggingAction,
        const="DEBUG",
        help="enable debugging messages",
    )
    parser.add_argument(
        "--prometheus-host",
        type=str,
        default="::1",
        help="Prometheus listen address, default: %(default)s",
    )
    parser.add_argument(
        "--prometheus-port",
        type=int,
        default=10240,
        help="Prometheus port, default: %(default)s",
    )
    parser.add_argument(
        "--receiver-host",
        type=str,
        default="::1",
        help="Reveiver listen address, default: %(default)s",
    )
    parser.add_argument(
        "--receiver-port",
        type=int,
        default=10241,
        help="Receiver port, default: %(default)s",
    )

    args = parser.parse_args()

    start_http_server(args.prometheus_port, args.prometheus_host)
    run(host=args.receiver_host, port=args.receiver_port)
